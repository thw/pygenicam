from lxml import etree

from genicam.core import ParseException, CommonEqualityMixin

'''
Every GenICam.Node property is implemented with an Element that maps to an xml
element. Element classes know how to translate (parse/render) to 
lxml.etree.Element. Elements only validate the format of the XML within the 
current Element. Nodes are responsible for doing validation between sub
elements. For example, a <Formula> node depends on <Variable> nodes being
defined as sibling nodes. Validation of siblings happens at the Node. 
'''

def num_string(num):
    ''' don't let python append L to numeric constants '''
    return str(num).rstrip('L')
    
class Element(CommonEqualityMixin):
    ''' 
    all members of every kind of Node described in the standard shall be 
    child classes of Element. valid Element leaf classes implement:
    
    def parse(self, etree_element):
        @param etree_element attempt to extract value from element
        @return bool true -> element was parsed successfully

    def render(self, etree_parent):
        @param etree_parent append to this element as a etree.SubElement
    '''
    def __init__(self, tag):
        ''' super(TODO_THIS_CLASS, self).__init__(tag) ''' 
        self.tag = tag
        
class AnyTreeElement(Element):
    ''' 
    <can/><be><any_arbitrary_xml></be>. 
    just store a copy of the etree 
    '''    
    def parse(self, etree_element):
        self.etree_element = etree_element
        return True

    def render(self, etree_parent):
        etree_parent.append(self.etree_element)

    def __init__(self, tag):
        super(AnyTreeElement, self).__init__(tag)

class StringElement(Element):
    ''' a string '''
    def parse(self, etree_element):
        self._string = etree_element.text
        return True

    def render(self, etree_parent):
        etree_elem = etree.SubElement(etree_parent, self.tag)
        etree_elem.text = self._string

    def __init__(self, tag):
        super(StringElement, self).__init__(tag)
        self._string = ''

    def set_val(self, v):
        self._string = str(v)

    def get_val(self):
        return self._string

class NumberElement(Element):
    ''' int formatted number '''
    def parse(self, etree_element):
        self._num = int(etree_element.text)
        return True

    def render(self, etree_parent):
        etree_elem = etree.SubElement(etree_parent, self.tag)
        etree_elem.text = num_string(self._num)

    def __init__(self, tag):
        super(NumberElement, self).__init__(tag)
        self._num = ''

    def set_val(self, v):
        self._num = v

    def get_val(self):
        return self._num


class FloatNumberElement(Element):
    ''' flpat formatted number '''
    def parse(self, etree_element):
        self._num = float(etree_element.text)
        return True

    def render(self, etree_parent):
        etree_elem = etree.SubElement(etree_parent, self.tag)
        etree_elem.text = str(self._num)

    def __init__(self, tag):
        super(FloatNumberElement, self).__init__(tag)
        self._num = 0.0

    def set_val(self, v):
        self._num = v

    def get_val(self):
        return self._num

class HexNumberElement(Element):
    ''' hex formatted number '''
    def parse(self, etree_element):
        self._num = int(etree_element.text, 16)
        return True

    def render(self, etree_parent):
        etree_elem = etree.SubElement(etree_parent, self.tag)
        etree_elem.text = num_string(hex(self._num))

    def __init__(self, tag):
        super(HexNumberElement, self).__init__(tag)
        self._num = ''

    def set_val(self, v):
        self._num = v

    def get_val(self):
        return self._num

def get_access_levels():
    return ['RO', 'WO', 'RW']
def get_display_notations():
    return ['Automatic', 'Fixed', 'Scientific']
def get_endianesses():
    return ['LittleEndian', 'BigEndian']
def get_signs():
    return ['Unsigned', 'Signed']
def get_representations():
    return ['Linear', 'Logarithmic', 'Boolean', 'PureNumber', 'HexNumber',
            'IPV4Address', 'MACAddress']
def get_visibility_levels():
    ''' list of visibilitiy levels sorted from most to least visible '''
    return ['Beginner', 'Expert', 'Guru', 'Invisible']
    
class StringListElement(Element):
    ''' only accept values in _string_list '''
    def parse(self, etree_element):
        val = etree_element.text
        if val in self._string_list:
            self._string = val
            return True
        return False

    def render(self, etree_parent):
        etree_elem = etree.SubElement(etree_parent, self.tag)
        etree_elem.text = self._string

    def __init__(self, tag):
        super(StringListElement, self).__init__(tag)
        self._string = ''
        self._string_list = []

    def set_val(self, v):
        if v not in self._string_list:
            raise ParseException('{} is not in {}'.format(v, self._string_list))
        self._string = v

    def get_val(self):
        return self._string

class AccessElement(StringListElement):
    def __init__(self, tag):
        super(AccessElement, self).__init__(tag)
        self._string_list = get_access_levels()

class DisplayNotationElement(StringListElement):
    def __init__(self, tag):
        super(DisplayNotationElement, self).__init__(tag)
        self._string_list = get_display_notations()

class EndianessElement(StringListElement):
    def __init__(self, tag):
        super(EndianessElement, self).__init__(tag)
        self._string_list = get_endianesses()

class RepresentationElement(StringListElement):
    def __init__(self, tag):
        super(RepresentationElement, self).__init__(tag)
        self._string_list = get_representations()

class SignElement(StringListElement):
    def __init__(self, tag):
        super(SignElement, self).__init__(tag)
        self._string_list = get_signs()

class VisibilityElement(StringListElement):
    def __init__(self, tag):
        super(VisibilityElement, self).__init__(tag)
        self._string_list = get_visibility_levels()

class NodeRefElement(Element):
    ''' a string keying another node's Name '''
    def parse(self, etree_element):
        ref = etree_element.text
        if ref is not None and len(ref) > 0:
            self._string = ref
            return True
        return False

    def render(self, etree_parent):
        etree_elem = etree.SubElement(etree_parent, self.tag)
        etree_elem.text = self._string

    def __init__(self, tag, str_val=''):
        super(NodeRefElement, self).__init__(tag)
        self._string = str_val
    
    def set_val(self, v):
        self._string = v

    def get_val(self):
        return self._string

