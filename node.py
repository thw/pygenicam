from genicam.core import ParseException, rm_xmlns, CommonEqualityMixin
from genicam.element import AnyTreeElement, StringElement, VisibilityElement
from genicam.element import NumberElement, HexNumberElement, FloatNumberElement
from genicam.element import AccessElement, NodeRefElement, SignElement
from genicam.element import RepresentationElement, DisplayNotationElement
from genicam.element import EndianessElement

#from UserDict import DictMixin

from lxml import etree

'''
This module defines all Node types.
'''

import collections



class Node(CommonEqualityMixin):
    ''' 
    A GenICam Node is Name, Description, Visibility, pValue and Value
    
    self.tag refers to the actual XML <tag> rendered and should be overriden
    by child classes. 
    
    self._elements refers to the child <tags> that should exist in the XML 
    file. Note that an _elements may be another Node. Some tags are allowed
    to have multiple elements within a single Node, such as EnumEntry, 
    Variable, pFeature, etc. These are stored as a list
    
    See GenICam standard section 2.8.1 
    '''
    def __len__(self):
        return len(self._elements)
    def __iter__(self):
        for i in self._elements:
            yield i
            
    def __init__(self):
        self.tag = 'Node'
        self.name = ''
        self.valid_elements = {   
                'Extension': {'type': AnyTreeElement, 'multiple': False}, 
                'ToolTip': {'type': StringElement, 'multiple': False}, 
                'Description': {'type': StringElement, 'multiple': False}, 
                'DisplayName': {'type': StringElement, 'multiple': False}, 
                'Visibility': {'type': VisibilityElement, 'multiple': False}, 
                'EventID': {'type': HexNumberElement, 'multiple': False},
                'pIsImplemented': {'type': NodeRefElement, 'multiple': False}, 
                'pIsAvailable': {'type': NodeRefElement, 'multiple': False}, 
                'pIsLocked': {'type': NodeRefElement, 'multiple': False}, 
                'pError': {'type': StringElement, 'multiple': False},
                'ImposedAccessMode': {'type': AccessElement, 'multiple': False}, 
                'pAlias': {'type': NodeRefElement, 'multiple': False},
                }
        self.required_elements = []
        self._elements = {}
    
    def has(self, sub_tag):
        ''' @return bool val = if self contains a subelement with sub_tag '''
        return sub_tag in self._elements

    def remove_element(self, elem_tag, elem_val):
        ''' removes the <tag>val</tag> subelement of this node '''
        if not self.has(elem_tag): 
            raise ValueError('remove_element called with invalid tag. no such element to remove.')

        tag_info = self.valid_elements[elem_tag]
        if tag_info['multiple']:
            val_filter = lambda se: se.get_val() != elem_val
            self._elements[elem_tag] = filter(val_filter, self._elements[elem_tag])
        else:
            self._elements.pop(elem_tag)


    def insert_element(self, elem):
        tag = elem.tag
        tag_info = self.valid_elements[tag]
        if tag_info['multiple']:
            if tag in self._elements:
                self._elements[tag].append(elem)
            else:
                self._elements[tag] = [elem]
        else:
            self._elements[tag] = elem

    def add_element(self, elem_tag, val):
        ''' check if elem_tag is valid, set it to val '''
        tag_info = self.valid_elements[elem_tag]
        
        if self.has(elem_tag) and not tag_info['multiple']:
            raise KeyError('No duplicate <{}> allowed.'.format(elem_tag))
        
        elem = tag_info['type'](elem_tag)
        elem.set_val(val)
        self.insert_element(elem)

    def add_elements(self, node_dict):
        ''' process dict items as <key>val</key> '''
        for key, val in node_dict.items():
            if key in self.valid_elements:
                if self.valid_elements[key]['multiple']:
                    for v in val:
                        self.add_element(key, v)
                else:
                    self.add_element(key, val)

    def render(self, etree_parent):
        ''' attach self and all sub_elements to the parent '''
        node = etree.SubElement(etree_parent, self.tag)
        node.attrib['Name'] = self.name
        for se in self.iter_elements():
            se.render(node)
        return node

    def parse(self, etree_element):    
        ''' 
        attempt to treat etree_element as a Node 
        @return bool True unless tag doesn't match, even if some
                elements didnt successfully parse by any supported type
        '''
        if rm_xmlns(etree_element.tag) != self.tag:
            return False
        self.name = etree_element.attrib['Name']
        for etree_sub in etree_element:
            tag = rm_xmlns(etree_sub.tag)
            if tag not in self.valid_elements:
                continue

            tag_info = self.valid_elements[tag]
            
            if self.has(tag) and not tag_info['multiple']:
                continue 
                #raise KeyError('No duplicate <{}> allowed. From {}'.format(tag, self.name))
            
            elem = tag_info['type'](tag)
            if elem.parse(etree_sub):
                self.insert_element(elem)
        for req in self.required_elements:
            if req not in self.keys():
                raise ParseException('Required element {} not found in {}'.format(req, tag))
        return True

    def iter_elements(self):
        ''' d = {k0: v, k1: [x, x, x], k2: [y]} '''
        for k, v in self._elements.items():
            if isinstance(v, collections.Iterable):
                for x in v:
                    yield x
            else:
                yield v
    
    def keys(self):
        ''' filter with set() to remove multiples '''
        return self._elements.keys()

    def __getitem__(self, key):
        ''' 
        node[key] returns the value in the <key>value</key> element 
        for entries that are valid to be multiple, return a list
        '''
        if self.valid_elements[key]['multiple']:
            gathered = [se.get_val() for se in self._elements[key]]
            if len(gathered) == 0:
                raise KeyError('no {} element in node {}'.format(key, self.name))
            return gathered
        for se in self.iter_elements():
            if se.tag == key:
                return se.get_val()
        raise KeyError('no {} element in node {}'.format(key, self.name))

    def __setitem__(self, key, value):
        '''
        node[key] = val inserts key or modifies current key value
        for multiple entry elements, expect a list of items to pass to set_val
        acqMode = Enumeration()
        acqMode['EnumEntry'] = [('Cont', 0), ('Single', 1)]
        when passed a multiple list, empty the current list and use add_element
        '''
        if self.valid_elements[key]['multiple']:
            self._elements[key] = []
            for v in value:
                self.add_element(key, v)
        else:
            for se in self.iter_elements():
                if se.tag == key:
                    se.set_val(value)
                    break
            else:
                self.add_element(key, value)
                

class Register(Node):
    '''
    The Register node maps to a contiguous array of bytes in the register space
    of the camera. The Register node implements the IRegister interface and 
    inherits its elements and attributes from the Node node. It in turn leaves 
    its elements and nodes to all specialized register access nodes, such as 
    IntReg, StringReg, etc. A Register node, however, can also be instantiated
    on its own giving access to the raw binary data.
    '''
    def __init__(self):
        super(Register, self).__init__()
        self.valid_elements.update({
                'Address': {'type': HexNumberElement, 'multiple': False},
                'Length': {'type': NumberElement, 'multiple': False},
                'AccessMode': {'type': AccessElement, 'multiple': False},
                'pPort': {'type': NodeRefElement, 'multiple': False},
                #TODO 'Cachable': {'type': ___Element, 'multiple': False},
                #TODO 'PollingTime': {'type': ___Element, 'multiple': False},
                #TODO 'pAddress': {'type': ___Element, 'multiple': False},
                #TODO 'pIndex': {'type': ___Element, 'multiple': False},
                #TODO 'pInvalidator': {'type': ___Element, 'multiple': False},
                })
        self.tag = 'Register'

class IntReg(Register):
    '''
    The IntReg node maps to byte-aligned integer registers. It inherits the 
    elements and attributes from Register nodes. Below is an example mapping
    to a 2 byte unsigned integer. Note that such a variable has the following 
    restriction parameters: Minimum = 0, Maximum = 65.535, Increment = 1. 
    '''
    def __init__(self):
        super(IntReg, self).__init__()
        self.valid_elements.update({
                'Sign': {'type': SignElement, 'multiple': False},
                'Endianess': {'type': EndianessElement, 'multiple': False},
                #TODO 'pIndex': {'type': ___Element, 'multiple': False},
                #TODO 'ValueIndexed': {'type': ___Element, 'multiple': False},
                #TODO 'pValueDefault': {'type': ___Element, 'multiple': False},
                #TODO 'pInvalidator': {'type': ___Element, 'multiple': False},
                })
        self.tag = 'IntReg'

class Integer(Node):
    '''
    '''
    def __init__(self):
        super(Integer, self).__init__()
        self.valid_elements.update({
                'Min': {'type': NumberElement, 'multiple': False},
                'Max': {'type': NumberElement, 'multiple': False},
                'Inc': {'type': NumberElement, 'multiple': False},
                'Value' : {'type': NumberElement, 'multiple': False, 'conflict': 'pValue' },
                'pValue': {'type': NodeRefElement, 'multiple': False, 'conflict': 'Value' },
                'Unit': {'type': StringElement, 'multiple': False},
                'Representation': {'type': RepresentationElement, 'multiple': False},
                #TODO 'Value': {'type': ___Element, 'multiple': False},
                #TODO 'pValueCopy': {'type': ___Element, 'multiple': False},
                })
        self.tag = 'Integer'


class FloatReg(Register):
    '''
    The FloatReg node maps to byte-aligned integer registers. It inherits the 
    elements and attributes from Register nodes. Below is an example mapping
    to a 2 byte unsigned integer. Note that such a variable has the following 
    restriction parameters: Minimum = 0, Maximum = 65.535, Increment = 1. 
    '''
    def __init__(self):
        super(FloatReg, self).__init__()
        self.valid_elements.update({
                'Endianess': {'type': EndianessElement, 'multiple': False},
                #TODO 'pIndex': {'type': ___Element, 'multiple': False},
                #TODO 'ValueIndexed': {'type': ___Element, 'multiple': False},
                #TODO 'pValueDefault': {'type': ___Element, 'multiple': False},
                #TODO 'pInvalidator': {'type': ___Element, 'multiple': False},
                })
        self.tag = 'FloatReg'

class Float(Node):
    def __init__(self):
        super(Float, self).__init__()
        self.tag = 'Float'
        self.valid_elements.update({
                'Min': {'type': FloatNumberElement, 'multiple': False},
                'Max': {'type': FloatNumberElement, 'multiple': False},
                'Inc': {'type': FloatNumberElement, 'multiple': False},
                'pValue': {'type': NodeRefElement, 'multiple': False, 'conflict': 'Value'},
                'Unit': {'type': StringElement, 'multiple': False},
                'Representation': {'type': RepresentationElement, 'multiple': False},
                'DisplayNotation': {'type': DisplayNotationElement, 'multiple': False},
                'DisplayPrecision': {'type': NumberElement, 'multiple': False},
                'Value': {'type': FloatNumberElement, 'multiple': False, 'conflict': 'pValue'},
                #TODO 'pValueCopy': {'type': ___Element, 'multiple': False},
                })

class StringReg(Register):
    def __init__(self):
        super(StringReg, self).__init__()
        self.tag = 'StringReg'

class EnumEntry(Node):
    '''
    This is an interesting Node that would be a good model for Variable.
    EnumEntry gets duck typed as both a Node._elements and a Root._nodes
    parse() and render() share the same xml interface in either list
    list, but when manipulating the XML DOM, set_val() and get_val() which
    are only called on Elements, not Nodes. 
    '''
    def __init__(self):
        super(EnumEntry, self).__init__()
        self.tag = 'EnumEntry'
        self.valid_elements = {
                'Value': {'type': NumberElement, 'multiple': False},
                }

    def get_val(self):
        return (self.name, self['Value'])

    def set_val(self, val):
        self.name = val[0]
        self['Value'] = val[1]


class Enumeration(Node):
    def __init__(self):
        super(Enumeration, self).__init__()
        self.tag = 'Enumeration'
        ee_type = lambda some_tag: EnumEntry()
        self.valid_elements.update({
                'pValue': {'type': NodeRefElement, 'multiple': False},
                'EnumEntry': {'type': ee_type, 'multiple': True},
                })

class Command(Node):
    def __init__(self):
        super(Command, self).__init__()
        self.tag = 'Command'
        self.valid_elements.update({
                'pValue': {'type': NodeRefElement, 'multiple': False},
                'CommandValue': {'type': NumberElement, 'multiple': False},
                })

class Boolean(Node):
    def __init__(self):
        super(Boolean, self).__init__()
        self.tag = 'Boolean'
        self.valid_elements.update({
                'pValue': {'type': NodeRefElement, 'multiple': False},
                'OnValue': {'type': NumberElement, 'multiple': False},
                'OffValue': {'type': NumberElement, 'multiple': False},
                })

class Port(Node):
    def __init__(self):
        super(Port, self).__init__()
        

'''
TODO: more Node types. 
String
MaskedIntReg
StructReg
SwissKnife
IntSwissKnife
Converter
IntConverter
ConfRom
TextDesc
IntKey
DcamLock
SmartFeature
Port
'''
def make_node(tag):
    type_map = { 
            'Node': Node,
            'Register': Register,
            'Integer': Integer,
            'IntReg': IntReg,
            'Boolean': Boolean,
            'Command': Command,
            'Float': Float,
            'FloatReg': FloatReg,
            'Enumeration': Enumeration,
            'EnumEntry': EnumEntry,
            'StringReg': StringReg,
            }
    return type_map[tag]()

def register_for_tag(feature_tag):
    tag_map = { 
            'Integer': 'IntReg',
            'Boolean': 'IntReg',
            'Command': 'IntReg',
            'Float': 'FloatReg',
            'Enumeration': 'IntReg',
            }
    return tag_map[feature_tag]

def make_register(tag):
    return make_node(register_for_tag(tag))

