# pygenicam

XML object helper classes for reading and writing GenICam Camera Description Files. 

## Usage

```
from pygenicam import Root 
mock = Root()
mock.root_attrib['VendorName'] = 'Foo'
mock.root_attrib['ModelName'] = 'Mock'

mock.add_category('DeviceInformation')
mock.add_node('DeviceInformation', 
        { 'tag': 'StringReg',
          'Name': 'DeviceVendorName',
          'Length': 256,
          'AccessMode': 'RO'
        })
mock.add_node('DeviceInformation', 
        { 'tag': 'StringReg',
          'Name': 'DeviceModelName',
          'Length': 256,
          'AccessMode': 'RO'
        })
mock.add_node('DeviceInformation', 
        { 'tag': 'StringReg',
          'Name': 'DeviceID',
          'Length': 256,
          'AccessMode': 'RO'
        })
mock.add_node_and_register('DeviceInformation',
        {   'tag': 'Enumeration',
            'Name': 'DeviceState',
            'Length': 4,
            'Sign': 'Unsigned',
            'Description': 'This feature provides the current state of the device.',
            'EnumEntry': [('Idle', 0), ('AcquireContinuous', 1), ('AcquireSingle', 2), ('AcquireBatch', 3), ('Error', 4)],
            'AccessMode': 'RO'
        })


with open('xml/foo_mock.xml', 'wb') as xml_file:
    xml_file.write(mock.render())
```