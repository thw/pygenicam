'''
genicam.core

This module contains core elements, things multiple modules need and 
breaks circular dependancies. See genicam.root for Root class
'''
class ParseException(Exception):
    pass

def rm_xmlns(s):
    ''' 
    lxml.etree.Element.tag generally looks like {Namespace}Tag
    TODO: handle namespace in documents. until then, provide this helper
    to strip namespaces off
    '''
    return s[s.rfind('}')+1:]


class CommonEqualityMixin(object):

    def __eq__(self, other):
        return (isinstance(other, self.__class__)
                and self.__dict__ == other.__dict__)

    def __ne__(self, other):
        return not self.__eq__(other)

