import uuid
from lxml import etree

from genicam.node import make_node, make_register, register_for_tag
from genicam.node import Node, Register
from genicam.element import NodeRefElement
from genicam.core import ParseException, rm_xmlns, CommonEqualityMixin

'''
GenICam

Pythonic interface to the GenICam standard. Includes tools for 
constructing and parsing GenICam XML files. Class structure reflects the
Node types described in the GenICam standard. The features and registers 
are ctypes aware so that they can be used by a pythonic GenTL wrapper. 

'''

class Category(Node):
    ''' 
    a list of feature nodes. Let Node handle the <Category> element and
    special case the Category's sub-Nodes by overriding render and parse
    '''
    def __init__(self):
        super(Category, self).__init__()
        self.tag = 'Category'
        # don't use += here, there is only one valid sub element
        self.valid_elements = { 
                'pFeature': {'type': NodeRefElement, 'multiple': True}, 
                }
        self.nodes = []
        
    def add_node(self, node):
        '''
        @param [in] node - a Node created for this category
        '''
        p_feat = NodeRefElement('pFeature', node.name)
        self.insert_element(p_feat)
        self.nodes.append(node)
                
    def render(self, etree_root):
        ''' 
        render a <Group><Category/> [self.nodes]</Group>
        Node will make the <Category/>
        '''
        g = etree.SubElement(etree_root, 'Group')
        g.attrib['Comment'] = self.name
        c = super(Category, self).render(g)

        for n in self.nodes:
            if not 'deprecated' in dir(n) or not n.deprecated:
                n.render(g)
            else:
                print('not rendering deprecated {}'.format(n))

    def parse(self, etree_elem):
        ''' etree_elem should be a <Group> '''
        if rm_xmlns(etree_elem.tag) != 'Group':
            raise ParseException('expect a <Group>')
        for se in etree_elem:
            tag = rm_xmlns(se.tag)
            if tag == 'Category':
                super(Category, self).parse(se)
            else:
                n = make_node(tag)
                n.category = self.name
                if n.parse(se):
                    self.nodes.append(n)
                else:
                    raise ParseException('failed to parse {n} from {se}'.format(n=n, se=se))
        return True

    def __str__(self):
        return 'GenICam Category {} with {} features'.format(self.name, len(self.features))

class Root(CommonEqualityMixin):
    ''' 
    root element of the GenICam description 

    self.categories is a list of Categories. When parsing     
    self.sub_elements contains the few Nodes that don't fit into a larger 
    category such as <Port> and <Integer Name="TLParamsLocked">

    XML Manipulation: 
    Root provides mechanisms for manipulating the GenICam XML document
    itself with the following functions:
        add_category, add_node, get_node('Name')['pValue'] = 'OtherNodeName'
    ''' 
    def __init__(self, xml=None):
        '''
        if xml is present parse it, otherwise start with an empty document
        '''
        self.etree_root = None
        self.root_attrib = {}
        self.categories = []
        self.sub_elements = []

        self.root_attrib['SchemaMajorVersion'] = "1"
        self.root_attrib['SchemaMinorVersion'] = "1" 
        self.root_attrib['SchemaSubMinorVersion'] = "0" 
        self.root_attrib['ProductGuid'] = str(uuid.uuid4())
        self.root_attrib['VersionGuid'] = str(uuid.uuid4()) 
        self.root_attrib['xmlns'] = "http://www.genicam.org/GenApi/Version_1_1" 
        #TODO: 
        #lxml complains about these attributes:
        # self.root_attrib['xmlns:xsi'] = "http://www.w3.org/2001/XMLSchema-instance" 
 #       self.root_attrib['xsi:schemaLocation'] = "http://www.genicam.org/GenApi/GenApiSchema_Version_1_1.xsd"
        
        if xml:
            self.parse(xml)

    def parse(self, xml):
        '''
        read the contents of the xml string
        '''
        tree = etree.XML(xml)
        self.etree_root = tree
        self.root_attrib.update(self.etree_root.attrib)
        for elem in self.etree_root:
            tag = elem.tag[elem.tag.rfind('}') + 1:]
            if tag == 'Group':
                c = Category()
                if c.parse(elem):
                    self.categories.append(c)
            else:
                n = make_node(tag)
                if n.parse(elem):
                    self.sub_elements.append(n)
                    
    def iter_nodes(self):
        ''' iterate across the nodes '''
        for se in self.sub_elements:
            yield se
        for c in self.categories:
            yield c
            for n in c.nodes:
                yield n

    def get_node(self, name):
        ''' search for a node by name. first in direct subs, then in groups '''
        for se in self.iter_nodes():
            if name == se.name:
                return se
        return None
        
    def _get_reg_for_node(self, name):
        for se in self.iter_nodes():
            if 'pValue' in se and se['pValue'] == name:
                return se
        return None
    
    def render(self):
        ''' 
        write out all Nodes to an XML doc.
        return utf-8 encoded 
        
        ''' 
        root = etree.XML('<RegisterDescription/>')
        root.attrib.update(self.root_attrib)
        
        for c in self.categories:
            c.render(root)

        for se in self.sub_elements:
            se.render(root)

        xml_contents = etree.tostring(root, 
                pretty_print=True, 
                xml_declaration=True,
                encoding='utf-8')
        return xml_contents
       
    def add_category(self, name):
        '''
        creates a new category for features
        '''
        c = Category()
        c.name = name
        c.base_address = len(self.categories) * 0x100000
        self.categories.append(c)

    def add_node(self, category, node_dict):
        '''
        create a GenICam Node with properties corresponding to the keys and 
        values in node_dict. node_dict['tag'] must be a valid Node type e.g.
        'Node', 'IntReg', 'FloatReg', 'IntSwissKnife', etc. 
        
        @param category string of category name
        @param node_dict must contain tag and name keys. all other keys are
                parsed as <tag><key0>val0</key0><key1>val1</key1>[...]</tag> 
                keys 'EnumEntry': must be {'Name': 'foo', 'Value': 13 }
        '''
        c = self.get_node(category)
        if c is None:
            raise ValueError('Invalid category name.')

        # strip tag and name, which are not subelements
        tag = node_dict['tag']
        name = node_dict['Name']
        deprecated = 'deprecated' in node_dict and node_dict['deprecated']
        attrib_keys = ['tag', 'Name', 'deprecated'] 
        node_dict = {k: v for k, v in node_dict.items() if k not in attrib_keys}
        
        if tag.endswith('Reg') and 'Address' not in node_dict:
            regs = [r for r in c.nodes if isinstance(r, Register)]
            regs = sorted(regs, key=lambda r: r['Address'])
            start_addr = c.base_address
            if len(regs) > 0:
                last = regs[-1]
                start_addr = last['Address'] + last['Length']                
            node_dict['Address'] = start_addr
        
        n = make_node(tag)
        n.name = name
        n.category = category
        n.add_elements(node_dict)
        n.deprecated = deprecated
        c.add_node(n)
        
    def remove_category(self, cat_name):
        ''' remove the category and all sub nodes '''
        for c in self.categories:
            if c.name == cat_name:
                self.categories.remove(c)
                return
        raise ValueError('category not found')
        
    def remove_node(self, node_name):
        ''' remove the node and possible pFeature '''
        feat = self.get_node(node_name)
        if feat is None:
            raise ValueError('no node by name {}'.format(node_name))
        if 'pValue' in feat:
            reg = self.get_node(feat['pValue'])
            self._remove_node(reg)
        self._remove_node(feat)
        
    def _remove_node(self, node):
        if node in self.sub_elements:
            print('remove node {} from root.sub_elements'.format(node))
            self.sub_elements.remove(node)
            return
            
        for c in self.categories:
            if node in c.nodes:
                print('remove node {} from {}'.format(node, c.name))
                c.nodes.remove(node)
                c.remove_element('pFeature', node.name)
                return
        raise ValueError('node not found')
       
    def add_deprecated_register(self, category, node_dict):
        ''' 
        mark the nodes as deprecated to avoid putting them in
        the xml. 
        '''
        node_dict['deprecated'] = True
        self.add_node_and_register(category, node_dict);
        
    def add_node_and_register(self, category, node_dict):
        ''' 
        add two Nodes, one a feature node such as Integer, Float, etc, and
        also add the corresponding Register entry for the feature node. 
        
        @param category name of the category where both nodes will reside
        @param node_dict dict containing info on both nodes
        '''
        deprecated = 'deprecated' in node_dict and node_dict['deprecated']

        tag = node_dict['tag']
        name = node_dict['Name']
        reg_dict = {}
        reg_keys = ['Length', 'AccessMode', 'Address', 'Sign']
        reg_dict = {k: v for k, v in node_dict.items() if k in reg_keys}
        node_dict = {k: v for k, v in node_dict.items() if k not in reg_keys}

        reg_dict['deprecated'] = deprecated
        node_dict['deprecated'] = deprecated
        
        reg_dict['Name'] = name + 'Reg'
        reg_dict['tag'] = register_for_tag(tag)
        
        node_dict['pValue'] = reg_dict['Name']
        self.add_node(category, node_dict)
        self.add_node(category, reg_dict)
        

